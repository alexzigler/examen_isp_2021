public class Subiect1 {
}

class A {

}

class B extends A {
    private long t;
    private D d;

    public void b() {

    }
}

class C {
    public void metoda(B b) {

    }
}

class D {
    private F f = new F();
    private G g = new G();
    private E e;

    public D() {
        e = new E();
    }

    public void met1(int i) {

    }
}

class E {
    public void met1() {

    }
}

class F {
    public void n(String s) {

    }
}

class G {
    public double met3() {
        return 2.3;
    }
}