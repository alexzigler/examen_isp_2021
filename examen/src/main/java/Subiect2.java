import javax.swing.*;

public class Subiect2 {
}

class UserInterface extends JFrame {
    private JTextArea text1 = new JTextArea();
    private JTextArea text2 = new JTextArea();
    private JButton button = new JButton();

    UserInterface() {
        setLayout(null);
        setVisible(true);
        setTitle("User interface");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(10, 10, 600, 400);
        text1.setBounds(20, 20, 400, 100);
        text2.setBounds(20, 150, 400, 100);
        button.setBounds(20, 300, 100, 20);
        button.setText("Calculeaza");
        button.addActionListener(e -> {
            String userText = text1.getText();
            int len = userText.length();
            String aText = "";
            for (int i = 0; i < len; i++) {
                aText = aText + "a";
            }
            text2.setText(aText);
        });
        add(text1);
        add(text2);
        add(button);
    }

}

class Main {
    public static void main(String[] args) {
        new UserInterface();
    }
}
